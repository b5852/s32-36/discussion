// npm install jsonwebtoken

const jwt = require("jsonwebtoken");

// JSON Web Token
/*
	- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
	- Information is kept secure through the use of the secret code
	- Only the system that knows the secret code that can decode the encrypted information

	- Imagine JWT as a gift wrapping service that secures the gift with a lock
	- Only the person who knows the secret code can open the lock
	- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
	- This ensures that the data is secure from the sender to the receiver
*/


// Token Creation


/*
	
	-Analogy
		Pack the gift and provide a locak with a secret code as the key.
*/


const secret = "crushAkoNgCrushKo";

module.exports.createAccessToken = (user) => {
	// When the user logs in, a token will be created with the user's information
	// This will be used for the token payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// Syntax: jwt.sign(payload, secretOrPrivateKey, [options/callBackFunctions])

	return jwt.sign(data, secret, {});
}


// Token Verification

/*
	Analogy
		Receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered with.

*/

// Middleware function
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	console.log(typeof token);

	if (typeof token !== "undefined") {
		console.log(token);
		token = token.slice(7, token.length)
		console.log(token);

		// Syntax: jwt.verify(token, secret, [options/callBackFunction])

		return jwt.verify(token, secret, (err, data) =>{
			// if jwt is not valid
			if (err) {
				return res.send({auth: "Token Failed"});
			} 
			else{
				// the verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function.
				next();
			}
		})
	}
	else {
		return res.send({auth: "Failed"});
	}
}

module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
			token = token.slice(7, token.length);
			return jwt.verify(token, secret, (err, data)=>{
				// If JWT is not valid
				if(err){
					return null;
				}
				else{
					// Syntax: jwt.decode(token, [options])
					return jwt.decode(token, {complete: true}).payload;
				}
			})
		}
		else{
			return res.send({auth: "Failed"});
		}
}
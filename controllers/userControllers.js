const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Check if the email already exists
/*
	
	Steps:
		1. Use mongoose "find" method to find duplicate emails
		2. Use the ".then" method to send back a response to the front end application based on the result of find method.


*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		// reult is equal to an empty array ([]);
		// if result is greater than 0, user is found/already exists.
		if (result.length > 0) {
			return true;
		}
		// No Duplicate email found
		else {
			return false;
		}
	})
}

// User Registration
/*
	
	Steps:
		1. Create a new User object using the mongoose model and the information from the request body
		2. Make sure that the password is encrypted
		3. Save the new user to the databse


*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		// npm install bcrypt
		// Syntax: bcrypt.hashSync(dataToBeEncrypted, salt)
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNumber: reqBody.mobileNumber
	})
	return newUser.save().then((user, error) =>{
		if(error){
			console.log(error);
			return false;
		}
		else {
			// Returns the new user object saved in the database back to the postman
			console.log(user);
			return true;
		}

	})

}


// User login
/*
	Steps:
	1. Check the database if the user's email is registered.
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate and return a JSON Web Token if the user is successfully login and return if not

*/


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{

		// User does not exists
		if (result == null) {
			// return false; //or
			return "User does not exist!";
		}
		// User exists
		else {
			// Syntax: bcrypt.compareSync(data, encrypted);
			// reqBody.password & result.password
			// bcrypt.compareSync() return Boolean
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);


			// If the passwords match the result.
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)};

			}
			// Password did not match
			else {
				// return false; //or
				return "Password did not match!";
			}

		}
	})
}


// module.exports.userDetails = (reqBody) => {
// 	return User.findOne({_id: reqBody._id}).then((result, error) => {

// 		if(error){
// 			console.log(error);
// 			return false;
// 		}
// 		else{
// 			if (result != null) {
			
// 			let details = {
// 				password: ""
// 			};

// 			result.password = details.password

// 			return result;
// 		}

// 		else {
// 			return false;
// 		}
// 		}

		
// 	})
// }

// Activity Solution

module.exports.userDetails = (data) => {
	return User.findById(data.userId).then(result =>{
		result.password = '';

		return result;
	})
}

module.exports.enroll = async(data) =>{

	console.log(data);
		// true or false										// result
	let isUserUpdated = await User.findById(data.userId).then(userResultData => {
		// Add the courseId in the user's enrollment array
		userResultData.enrollments.push({courseId: data.courseId})

		// Saves the updated
		return userResultData.save().then((enrollment, error) =>{
			if(error){
				return false
			}
			else{
				return true;
			}
		})
	})
	console.log(isUserUpdated);

	let isCourseUpdated = await Course.findById(data.courseId).then(courseResultData => {
		courseResultData.enrollees.push({userId: data.userId})

		courseResultData.slots -= 1;

		return courseResultData.save().then((enrollees, error) =>{
			if(error){
				return false
			}
			else{
				return true;
			}
		})
	})
	console.log(isCourseUpdated);
	if(isUserUpdated && isCourseUpdated){
		return false
	}
	else{
		return true;
	}
}

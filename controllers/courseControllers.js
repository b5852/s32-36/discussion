const Course = require("../models/Course");
const auth = require("../auth");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body.
	2. Save the new User to the database.

*/


module.exports.addCourse = (reqBody) =>{
	

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})

	return newCourse.save().then((course, error) =>{
		if(error){
			return false
		}
		else{
			return true
		}
	})


}
// Retieve all Courses
/*
	Step:
	1. Retrieve all the courses from the database
*/

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => result);
}

// Retieve all ACTIVE Courses
/*
	Step:
	1. Retrieve all the courses from the database with the property of "isActive" to true
*/
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => result);
}
// Retrieving a specific course
/*
	Step:
	1. Retrieve the course that matches the courseID provided from the URL. 

*/


module.exports.getCourse = (courseId) =>{
	return Course.findById(courseId).then(result => result);
}

// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body.
	2. Find and update the course using the courseId retrieved from request params/url property and the variable "updatedCourse" containing  the information from the request body.

*/

module.exports.updateCourse = (courseId, reqBody) =>{
	// Specify the fields/properties to be updated

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots

	}

	// Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((courseUpdate, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

}

// Archieve a course

module.exports.archieveCourse = (courseId, reqBody) =>{
	// Specify the fields/properties to be updated

	let archievedCourse = {
		isActive: reqBody.isActive

	}

	// Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, archievedCourse).then((courseArchive, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

}



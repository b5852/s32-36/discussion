const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");


// route for creating a course

router.post("/", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization); //contains the token
	console.log(`userData Result: ${userData}`)
	// if isAdmin is equal to true 
	if (userData.isAdmin) {
			courseControllers.addCourse(req.body).then(resultFromController => res.send(resultFromController));
		}
		// isAdmin is equal to false
		else {
			res.send("You don’t have permission on this page!");
		}

	
})
//Route for retrieving all the courses
router.get("/all", auth.verify, (req, res) =>{
			courseControllers.getAllCourses().then(resultFromController => res.send(resultFromController));
		
})

//Route for retrieving all active courses
router.get("/", (req, res) =>{
			courseControllers.getAllActive().then(resultFromController => res.send(resultFromController));
		
})

// Route for retrieving a specific course
router.get("/:courseId", (req, res) =>{
	console.log(req.params.courseId);
			courseControllers.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController));
		
})


// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) =>{
	console.log(req.params.courseId);
			courseControllers.updateCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController));
		
})


// Route to archieve a course
router.patch("/:courseId/archieve", auth.verify, (req, res) =>{
	console.log(req.params.courseId);
	courseControllers.archieveCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;
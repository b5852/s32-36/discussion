
const express = require("express");

// Allows access to Http method middlewares that makes it easier to create routes for our applcaition
const router = express.Router();


// The "userControllers" allow us to use the function defined inside it
const userControllers = require("../controllers/userControllers");

const auth = require("../auth");


// Router for checking if the email exists
router.post("/checkEmail", (req, res) =>{
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Router for the user registration
router.post("/register", (req, res) =>{
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for the user login (with token creation)
router.post("/login", (req, res) =>{
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


// Route for the user login (with token creation)
// router.post("/details", (req, res) =>{
// 	// Provides the user's ID for the getPorfile controller method
// 	userControllers.userDetails(req.body).then(resultFromController => res.send(resultFromController))
// })

// Activity Solution 
router.get("/details", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization); //contains the token

	console.log(userData);

	// Provides the user's ID for the getPorfile controller method
	userControllers.userDetails({userId: userData.id}).then(resultFromController => res.send(resultFromController))
})

// Route to enroll a course
router.post("/enroll", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	let data = {
		// userId will ne retrieved from the request header
		userId: userData.id,
		courseId: req.body.courseId
	}
	if (userData.isAdmin) {
		res.send("You're not allowed to access this page")
	}
	else{
		userControllers.enroll(data).then(resultFromController => res.send(resultFromController))
	}
	
})


// Use "module.exports" to export the router object to be use in the server
module.exports = router;
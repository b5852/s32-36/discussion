/*
	Setup the dependencies

	To the terminal (gitbash)

		npm init -y
		
		npm install express mongoose
		or
		npm install express
		npm install mongoose

		create a new file with the filename: ".gitignore" and contain "/node_module" then save the file

		MongoDB myHelloWorld Cluster
		mongodb+srv://system:<password>@myhelloword.t5o0pyw.mongodb.net/?retryWrites=true&w=majority

		npm install cors

*/

// Require modules 
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


const userRoutes = require("./routes/userRoutes") 
const courseRoutes = require("./routes/courseRoutes") 

// Create server
const app = express();

// port
const port = 4000;

// Connect to our MongoDB Database 
mongoose.connect("mongodb+srv://system:admin@myhelloword.t5o0pyw.mongodb.net/course-booking-app?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

// Set Notification for connection success or failure
let db = mongoose.connection;

// if failed to connect the database
db.on("error", console.error.bind(console, "Connection Error"));

// if database connection successfully connected
db.once("open", () => console.log("We're connected to the cloud database"));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Routes for our API
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// listening to port
// This syntax will allow flexibility when using the application locally or as a hosted application (online)
app.listen(process.env.PORT || port, () => {
	// 
	console.log(`API is now online on port ${process.env.PORT || port}`)
})




